import { render, screen } from '@testing-library/react';
import App from './App';

test('intro', () => {
  render(<App />);
  const text = screen.getByText(/High interest rates./i);
  expect(text).toBeInTheDocument();
});
