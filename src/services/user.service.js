import { HEADERS, API_URL } from '../constants/APIConfig'
import axios from 'axios'

export const userService = {
  login,
  logout,
  profile,
  isAuthenticated,
  updateProfile,
}

function login (username, password) {
  const requestOptions = {
    method: 'POST',
    headers: HEADERS(),
    body: JSON.stringify({ 'email': username, 'password': password }),
  }
  return fetch(`${API_URL}/user/login`, requestOptions).
    then(handleResponse).
    then(response => {
        if (response.status === 200) {
          console.log(response.message)
          const user = {
            username: username,
            token: response.body.token ? response.body.token : '',
          }
          return this.profile(user).then(
            userInfo => {
              const myUser = {
                ...user,
                ...userInfo,
              }
              // store user details and jwt token in local storage to keep user logged in between page refreshes
              localStorage.setItem('user', JSON.stringify(myUser))
              return myUser
            },
            error => Promise.reject(error),
          )
        }
      },
      error => Promise.reject(error),
    )
}

function updateProfile (user, firstName, lastName) {
  const requestOptions = {
    method: 'PUT',
    headers: HEADERS(user.token),
    body: JSON.stringify({ 'firstName': firstName, 'lastName': lastName }),
  }
  return fetch(`${API_URL}/user/profile`, requestOptions).
    then(handleResponse).
    then(response => {
        if (response.status === 200) {
          console.log(response.message)
          const userInfo = response.body
          const myUser = {
            ...user,
            ...userInfo,
          }
          localStorage.setItem('user', JSON.stringify(myUser))
          return myUser
        }
      },
      error => Promise.reject(error),
    )
}

function profile (user = null) {
  if (!user) return null
  const requestOptions = {
    method: 'POST',
    headers: HEADERS(user.token),
  }
  return fetch(`${API_URL}/user/profile`, requestOptions).
    then(handleResponse).
    then(response => {
      if (response.status === 200) {
        console.log(response.message)
        const userinfo = response.body
        localStorage.setItem('user', JSON.stringify({
          ...user,
          ...userinfo,
        }))

        return response.body
      }
    })
}

function isAuthenticated () {
  const json = localStorage.getItem('user')
  const user = json ? JSON.parse(json) : null
  return (user && user.token) ? user : null
}

function logout () {
  // remove user from local storage to log user out
  localStorage.removeItem('user')
  /*console.log('before logout - localStorage:', localStorage.getItem('user'))*/
}

function handleResponse (response) {
  return response.text().then(
    text => {
      const data = text && JSON.parse(text)
      if (!response.ok) {
        if (response.status === 401) {
          // auto logout if 401 response returned from api
          logout()
          window.location.reload(true)
        }

        const error = (data && data.message) || response.statusText
        return Promise.reject(error)
      }

      return data
    })
}
