import { ACTION_TYPES } from '../constants'
import { userService } from '../services'
import store from '../store'

export const userActions = {
  login,
  logout,
  getProfile,
  isAuthenticated,
  onEditProfile,
  updateProfile,
}

function isAuthenticated () {
  return dispatch => {
    let user = store.getState().authentication.user
    user = user ? user : userService.isAuthenticated()
    user && dispatch(success(user))
    return user
  }

  function success (user) {
    return {
      type: ACTION_TYPES.USER_AUTHENTICATED,
      user,
    }
  }
}

function onEditProfile () {
  return dispatch => {
    const isEditProfile = !(store.getState().authentication.isEditProfile
      ? true
      : false)
    dispatch({ type: ACTION_TYPES.SET_VISIBILITY_EDIT_PROFILE, isEditProfile })
  }
}

function updateProfile (user, firstName, lastName) {
  return dispatch => {
    dispatch(request())
    userService.updateProfile(user, firstName, lastName).then(
      user => {
        dispatch(success(user))
        console.log('profile success ', success(user))
      },
      error => {
        dispatch(failure(error))
        console.error('profile failure')
      },
    )
  }

  function request () { return { type: ACTION_TYPES.PROFILE_REQUEST } }

  function success (user) {
    return {
      type: ACTION_TYPES.PROFILE_SUCCESS,
      user,
    }
  }

  function failure (error) {
    return {
      type: ACTION_TYPES.PROFILE_FAILURE,
      error,
    }
  }
}

function login (props, username, password) {
  return dispatch => {
    dispatch(request({ username }))
    userService.logout()
    userService.login(username, password).then(
      user => {
        dispatch(success(user))
        console.log('success ', success(user))
        props.history.push('/user')
      },
      error => {
        dispatch(failure(error))
        console.error('login failure')
      },
    )
  }

  function request (user) { return { type: ACTION_TYPES.LOGIN_REQUEST, user } }

  function success (user) { return { type: ACTION_TYPES.LOGIN_SUCCESS, user } }

  function failure (error) {
    return {
      type: ACTION_TYPES.LOGIN_FAILURE,
      error,
    }
  }
}

function logout (props) {
  userService.logout()
  props.history.push('/')
  return { type: ACTION_TYPES.LOGOUT }
}

function getProfile (user) {
  return dispatch => {
    dispatch(request())
    userService.profile(user).then(
      user => dispatch(success(user)),
      error => dispatch(failure(error)),
    )
  }

  function request () { return { type: ACTION_TYPES.PROFILE_REQUEST } }

  function success (user) {
    return {
      type: ACTION_TYPES.PROFILE_SUCCESS,
      user,
    }
  }

  function failure (error) {
    return {
      type: ACTION_TYPES.PROFILE_FAILURE,
      error,
    }
  }
}
