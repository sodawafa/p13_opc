import { ACTION_TYPES } from '../constants'

let userCookie = JSON.parse(localStorage.getItem('user'))
let initialState = userCookie ? {
  loggedIn: true,
  user: userCookie,
} : {}
initialState = { ...initialState, isEditProfile: false }

export function authentication (state = initialState, action) {
  switch (action.type) {
    case ACTION_TYPES.LOGIN_REQUEST:
      return {
        loggingIn: true,
        user: action.user,
      }
    case ACTION_TYPES.LOGIN_SUCCESS:
      return {
        loggedIn: true,
        user: action.user,
      }
    case ACTION_TYPES.LOGIN_FAILURE:
      return {}
    case ACTION_TYPES.LOGOUT:
      return {}
    case ACTION_TYPES.USER_AUTHENTICATED:
      return {
        loggedIn: true,
        user: action.user,
      }
    case ACTION_TYPES.PROFILE_REQUEST:
      return {
        loading: true,
      }
    case ACTION_TYPES.PROFILE_SUCCESS:
      return {
        user: action.user,
        isEditProfile: false,
      }
    case ACTION_TYPES.PROFILE_FAILURE:
      return {
        error: action.error,
      }
    case ACTION_TYPES.SET_VISIBILITY_EDIT_PROFILE:
      return {
        ...state,
        isEditProfile: action.isEditProfile,
      }
    default:
      return state
  }
}
