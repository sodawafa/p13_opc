import { combineReducers } from "redux";

import { authentication } from './authentication.reducer';

const reducers = combineReducers({
  authentication,
});

export default reducers;
