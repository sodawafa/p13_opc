import React from 'react'
import { connect } from 'react-redux'
import { userActions } from '../actions'

class Logout extends React.Component {

  constructor (props) {
    super(props)
    this.props.dispatch(userActions.logout(props))
  }

  render () {
    return (<div></div>)
  }
}

const connectedLogoutPage = connect()(Logout)
export { connectedLogoutPage as Logout }

