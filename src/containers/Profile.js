import React from 'react'
import { connect } from 'react-redux'
import { userActions } from '../actions'
import '../assets/style/form-edit.css'

class Profile extends React.Component {

  constructor (props) {
    super(props)
    this.state = {
      firstName: this.props.user.firstName,
      lastName: this.props.user.lastName,
      submitted: false,
    }
    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  componentDidMount () {
  }

  handleChange (e) {
    const { name, value } = e.target
    this.setState({ [name]: value })
  }

  handleSubmit (e) {
    e.preventDefault()
    this.props.dispatch(
      userActions.updateProfile(
        this.props.user,
        this.state.firstName,
        this.state.lastName,
      ),
    )
  }

  render () {
    return (
      <form className={'edit-form'} onSubmit={this.handleSubmit}>
        <div>
          <input name={'firstName'} type={'text'} id={'firstName'}
                 defaultValue={this.props.user.firstName}
                 onChange={this.handleChange}/>
          <input name={'lastName'} type={'text'} id={'lastName'}
                 defaultValue={this.props.user.lastName}
                 onChange={this.handleChange}/>
        </div>
        <div>
          <button type={'submit'} className="save-button">Save</button>
          <button type={'reset'} className="reset-button">Cancel</button>
        </div>
      </form>
    )
  }
}

const mapStateToProps = state => ({
  user: state.authentication.user,
})

const connectedLoginPage = connect(mapStateToProps)(Profile)
export { connectedLoginPage as Profile }
