import React from 'react'
import { Link } from 'react-router-dom'
import store from '../store'
import { userService } from '../services'
import { userActions } from '../actions'
import { connect } from 'react-redux'

function Logo () {
  return <a className="main-nav-logo" href="/#">
    <img
      className={'main-nav-logo-image'}
      src="/img/argentBankLogo.png"
      alt="Argent Bank Logo"
    />
    <h1 className="sr-only">Argent Bank</h1>
  </a>
}

class Header extends React.Component {

/*
  state = {
    user: false,
  }
  componentDidMount () {
    this.setState({
      user: userService.isAuthenticated(),
    })
    store.subscribe(() => {
      const user = store.getState().authentication
        ? store.getState().authentication.user
        : false
      console.log('!user!', user)
      this.setState({
        user: user,
      })
    })
  }*/


  render () {
    if (!this.props.user)
      return (
        <nav className="main-nav">
          <Logo/>
          <div>
            <Link className="main-nav-item" to="/login">
              <i className="fa fa-user-circle"/>
              Sign In
            </Link>
          </div>
        </nav>
      )
    else
      return (
        <nav className="main-nav">
          <Logo/>
          <div>
            <Link className="main-nav-item" to="/user">
              <i className="fa fa-user-circle"/>
               {this.props.user.firstName}
            </Link>
            <Link className="main-nav-item" to="/logout">
              <i className="fa fa-sign-out"/>
              Sign Out
            </Link>
          </div>
        </nav>
      )
  }
}

/*export default Header*/

const mapStateToProps = state => ({
  user: state.authentication.user,
})

const connectedLoginPage = connect(mapStateToProps)(Header)
export { connectedLoginPage as Header }
