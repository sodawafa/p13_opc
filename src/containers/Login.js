import React from 'react'
import { connect } from 'react-redux'
import { userActions } from '../actions'

class Login extends React.Component {

  constructor (props) {
    super(props)

    this.state = {
      username: '',
      password: '',
      submitted: false,
    }
    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
    const user = this.props.dispatch(userActions.isAuthenticated())
    if (user) props.history.push('/user')
  }

  componentDidMount () {
  }

  handleChange (e) {
    const { name, value } = e.target
    /*console.log({ [name]: value })*/
    this.setState({ [name]: value })
  }

  handleSubmit (e) {
    e.preventDefault()
    this.setState({ submitted: true })
    const { username, password } = this.state
    const { dispatch } = this.props
    if (username && password) {
      dispatch(userActions.login(this.props, username, password))
    }
  }

  render () {

    const { loggingIn } = this.props
    const { username, password, submitted } = this.state
    return (
      <main className="main bg-dark">
        <section className="sign-in-content">
          {loggingIn &&
          <i className="fa fa-cog fa-spin fa-4x"/>
          }
          {!loggingIn &&
          <i className="fa fa-user-circle sign-in-icon"/>
          }
          <h1>Sign In</h1>
          <form onSubmit={this.handleSubmit}>
            <div className="input-wrapper">
              <label htmlFor="username">Username</label
              ><input type="text" id="username" name={'username'}
                      onChange={this.handleChange}/>
              {submitted && (username.length < 5) &&
              <div className="help-block">Username is required</div>
              }
            </div>
            <div className="input-wrapper">
              <label htmlFor="password">Password</label>
              <input type="password" id="password" name={'password'}
                      onChange={this.handleChange}/>
              {submitted && (password.length < 4) &&
              <div className="help-block">Password is required</div>
              }
            </div>
            <div className="input-remember">
              <input type="checkbox" id="remember-me"/>
              <label htmlFor="remember-me">Remember me</label>
            </div>
            <button type={'submit'} className="sign-in-button">Sign
              In
            </button>
          </form>
        </section>
      </main>
    )
  }
}
const mapStateToProps = state => ({
  loggingIn: state.authentication.loggingIn,
});


const connectedLoginPage = connect(mapStateToProps)(Login)
export { connectedLoginPage as Login }

