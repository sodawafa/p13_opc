import React, { Component } from 'react'
import { Link, Route } from 'react-router-dom'
import '../assets/style/notfound.css'


function Status({ code, children }) {
  return (
    <Route
      render={({ staticContext }) => {
        if (staticContext) staticContext.status = code;
        return children;
      }}
    />
  );
}

class Notfound extends Component {
  render () {
    return <Status code={404}>
      <div className={'undef'}>
        <h1>404</h1>
        <p>Oups! La page que vous demandez n'existe pas.</p>
        <Link to={"/"}>Retourner sur la page d'accueil</Link>
      </div>
    </Status>
  }
}
export default Notfound
