import './assets/style/style.css'
import {Header} from './containers/Header'
import Footer from './components/Footer'
import Notfound from './components/Notfound'
import Home from './components/Home'
import { Dashboard } from './containers/Dashboard'
import { Login } from './containers/Login'
import { Logout } from './containers/Logout'
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect,
} from 'react-router-dom'

import React from 'react'


function PrivateRoute ({ children, ...rest }) {
  let auth = localStorage.getItem('user')
  return (
    <Route
      {...rest}
      render={({ location }) =>
        auth ? (
          children
        ) : (
          <Redirect
            to={{
              pathname: '/login',
              state: { from: location },
            }}
          />
        )
      }
    />
  )
}

function App () {
  return (
    <Router>
      <Header/>
      <Switch>
        <Route exact path="/">
          <Home/>
        </Route>
        <PrivateRoute path="/user">
          <Dashboard/>
        </PrivateRoute>
        <Route path="/login" component={Login}/>
        <Route path="/logout" component={Logout}/>
        <Route path="*" component={Notfound}/>
      </Switch>
      <Footer/>
    </Router>
  )
}

export default App
